FROM python:3.8-slim-buster
WORKDIR /backend 
COPY requirements.txt ./
RUN pip install -r requirements.txt
RUN FLASK_APP=app.py
COPY . .
CMD ["flask", "run", "--host", "0.0.0.0", "--port", "5000"]
